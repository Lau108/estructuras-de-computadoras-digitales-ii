#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string.h>
#include <sstream>
#include "predictor.h"

using namespace std;

// Tipos de predictores
#define BIMODAL 0
#define GSHARE 1
#define PSHARE 2
#define TOURNAMENT 3

int main(int argc, char *argv[]) {
  // Parametros de linea de comandos
  int s = atoi(argv[2]);
  int bp = atoi(argv[4]);
  int gh = atoi(argv[6]);
  int ph = atoi(argv[8]);
  int o = atoi(argv[10]);

  // Variables para lectura de trace
  string PC;
  string outcome;
  string file;
  
  // Contador para 500 datos
  int count = 0;

  cout << "File Reading...\n" << endl;
  // Inicializar predictor
  Predictor predictor_branch(s, bp, gh, ph, o);
  // Escribir encabezado en archivo
  predictor_branch.print_r(PC, outcome, 1, o, bp);
  // Lectura del trace
  while(getline(cin,file)){
    istringstream iss (file);
    // contador de datos
    count++;
    iss >> PC >> outcome;
    // Seleccion de predictor
    switch (bp) {
      case BIMODAL:
          predictor_branch.Bimodal(PC, outcome);
          break;
      case GSHARE:
          predictor_branch.Gshare(PC, outcome);
          break;
      case PSHARE:
          predictor_branch.Pshare(PC, outcome);
          break;
      case TOURNAMENT:
          predictor_branch.Tournament(PC, outcome);
          break;
    }
    // Escritura de archivo y/o impresion
    if (count <= 5000) {
      predictor_branch.print_r(PC, outcome, 2, o, bp);
      }
    }
  // Resultados en terminal
  predictor_branch.print_r(PC, outcome, 3, o, bp);
  // Return 0 para correcta ejecucion
  return 0;
}
