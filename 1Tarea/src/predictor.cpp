#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include<vector>
#include "math.h"
#include "predictor.h"

// Estados
#define STRONGLY_NOT_TAKEN 0
#define WEAKLY_NOT_TAKEN 1
#define WEAKLY_TAKEN 2
#define STRONGLY_TAKEN 3
#define STRONGLY_PREFER_PSHARE 0
#define WEAKLY_PREFER_PSHARE 1
#define WEAKLY_PREFER_GSHARE 2
#define STRONGLY_PREFER_GSHARE 3

/** @brief Constructor del Predictor
*
*   @details Inicializa los valores, da tamano a los registros y seleccion
*   tipo de predictor.
*
*   @param s  Tamano de la tabla BTH
*   @param bp Tipo de prediccion
*   @param gh Tamano del registro de historia global
*   @param ph Tamano del registro de historia privada
*   @param o  Salida de simulacion
*/
Predictor::Predictor(int s, int bp, int gh, int ph, int o){
  /// Establece numero de entradas para el registro BTH
  this->entries_BTH = pow(2,s);
  /// Da tamanos a los registros
  this->BTH.resize(this->entries_BTH);
  this->BTHp.resize(this->entries_BTH);
  this->BTHg.resize(this->entries_BTH);
  this->PTH.resize(this->entries_BTH);
  this->MPT.resize(this->entries_BTH);
  /// Asegura un gh <= s && ph <= s
  this->size_BTH = s;
  if (s >= gh) {
    this->size_Gh = gh;
  } else {
    this->size_Gh = s;
  }
  if (s >= ph) {
    this->size_Ph = ph;
  } else {
    this->size_Ph = s;
  }
  /// Selecciona tipo de prediccion
  switch (bp) {
    case 0:
      this->predictor_type = "Bimodal";
      break;
    case 1:
      this->predictor_type = "Gshare";
      break;
    case 2:
      this->predictor_type = "Pshare";
      break;
    case 3:
      this->predictor_type = "Tournament";
      break;
    default:
      this->predictor_type = "Null";
      break;
  }
  /// Inicializa valores en 0
  this->state = 0;
  this->num_branch = 0;
  this->num_correct_taken = 0;
  this->num_correct_not_taken = 0;
  this->num_incorrect_taken = 0;
  this->num_incorrect_not_taken = 0;
  this->percentage = 0;
}

/** @brief Predictor Bimodal
*
*   @details Implementacion de predictor Bimodal
*
*   @param pc      Direccion de la instruccion de salto
*   @param outcome Caracter que indica si se toma o no el salto
*/
void Predictor::Bimodal(string pc, string outcome){
  // Contador de numero de saltos
  this->num_branch++;
  // Direccion de string a unsigned long long
  unsigned long long PC = stoull(pc);
  // Direccion de unsigned long long a 32 bits
  bitset<32> PC_32b(PC);
  // Se desplaza para obtener los s bits pasados por parametro
  PC_32b <<= 32 - this->size_BTH;
  PC_32b >>= 32 - this->size_BTH;
  // Nueva direccion a int
  int dir_BTH = (int)(PC_32b.to_ulong());
  // Acceso a estado en la direccion del registro BTH
  int state = this->BTH[dir_BTH];
  // Caso en que se toma el salto VS prediccion
  if (outcome == "T") {
    switch (state) {
      case STRONGLY_TAKEN:
              // Predictor se mantiene en STRONGLY_TAKEN
              this->BTH[dir_BTH] = STRONGLY_TAKEN;
              // Suma 1 a numero correcto de taken
              this->num_correct_taken = this->num_correct_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "T";
              break;
      case WEAKLY_TAKEN:
              // Predictor pasa a STRONGLY_TAKEN
              this->BTH[dir_BTH] = this->BTH[dir_BTH] + 1;
              // Suma 1 a numero correcto de taken
              this->num_correct_taken = this->num_correct_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "T";
              break;
      // Prediccion STRONGLY_NOT_TAKEN o WEAKLY_NOT_TAKEN
      default:
              // Prediccion pasa a WEAKLY_NOT_TAKEN o a WEAKLY_TAKEN
              this->BTH[dir_BTH] = this->BTH[dir_BTH] + 1;
              // Suma 1 a numero incorrecto de taken
              this->num_incorrect_not_taken = this->num_incorrect_not_taken + 1;
              // Prediccion incorrecta
              this->correct_or_incorrect = "incorrect";
              // Prediccion
              this->prediction = "N";
              break;
    }
  // Caso en que no toma el salto VS prediccion
  } else {
    switch (state) {
      case STRONGLY_NOT_TAKEN:
              // Prediccion se mantiene en STRONGLY_NOT_TAKEN
              this->BTH[dir_BTH] = STRONGLY_NOT_TAKEN;
              // Suma 1 a numero correcto de not taken
              this->num_correct_not_taken = this->num_correct_not_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "N";
              break;
      case WEAKLY_NOT_TAKEN:
              // Prediccion pasa a STRONGLY_NOT_TAKEN
              this->BTH[dir_BTH] = this->BTH[dir_BTH] - 1;
              // Suma 1 a numero correcto de not taken
              this->num_correct_not_taken = this->num_correct_not_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "N";
              break;
      // Prediccion STRONGLY_TAKEN o WEAKLY_TAKEN
      default:
              // Prediccion pasa a WEAKLY_TAKEN o a WEAKLY_NOT_TAKEN
              this->BTH[dir_BTH] = this->BTH[dir_BTH] - 1;
              // Suma 1 a numero incorrecto de taken
              this->num_incorrect_taken = this->num_incorrect_taken + 1;
              // Prediccion incorrecta
              this->correct_or_incorrect = "incorrect";
              // Prediccion
              this->prediction = "T";
              break;
    }
  }
  // Return vacio
  return;
}

/** @brief Predictor con historia Global
*
*   @details Implementacion de predictor con historia Global
*
*   @param pc      Direccion de la instruccion de salto
*   @param outcome Caracter que indica si se toma o no el salto
*/
void Predictor::Gshare(string pc, string outcome){
  // Contador de numero de saltos
  this->num_branch++;
  // Direccion de string a unsigned long long
  unsigned long long PC = stoull(pc);
  // Direccion de unsigned long long a 32 bits
  bitset<32> PC_32b(PC);
  // Se desplaza direccion para obtener los s bits pasados por parametro
  PC_32b <<= 32 - this->size_BTH;
  PC_32b >>= 32 - this->size_BTH;
  // Se desplaza historia global para obtener los gh bits pasados por parametro
  this->Ghistory <<= 32 - this->size_Gh;
  this->Ghistory >>= 32 - this->size_Gh;
  // XOR entre direccion e historia global
  bitset<32> dir_xor_32b = (PC_32b ^= this->Ghistory);
  // Nueva direccion a int
  int dir_xor = (int)(dir_xor_32b.to_ulong());
  // Acceso a estado en la direccion del registro BTHg
  int state = this->BTHg[dir_xor];
  // Caso en que se toma el salto VS prediccion
  if (outcome == "T") {
    // Actualizacion de historia global
    this->Ghistory <<= 1;
    this->Ghistory[0] = 1;
    switch (state) {
      case STRONGLY_TAKEN:
              // Predictor se mantiene en STRONGLY_TAKEN
              this->BTHg[dir_xor] = STRONGLY_TAKEN;
              // Suma 1 a numero correcto de taken
              this->num_correct_taken = this->num_correct_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "T";
              // Prediccion para torneo
              this->pred_G = 1;
              break;
      case WEAKLY_TAKEN:
              // Predictor pasa a STRONGLY_TAKEN
              this->BTHg[dir_xor] = this->BTHg[dir_xor] + 1;
              // Suma 1 a numero correcto de taken
              this->num_correct_taken = this->num_correct_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "T";
              // Prediccion para torneo
              this->pred_G = 1;
              break;
      // Prediccion STRONGLY_NOT_TAKEN o WEAKLY_NOT_TAKEN
      default:
              // Prediccion pasa a WEAKLY_NOT_TAKEN o a WEAKLY_TAKEN
              this->BTHg[dir_xor] = this->BTHg[dir_xor] + 1;
              // Suma 1 a numero incorrecto de taken
              this->num_incorrect_not_taken = this->num_incorrect_not_taken + 1;
              // Prediccion incorrecta
              this->correct_or_incorrect = "incorrect";
              // Prediccion
              this->prediction = "N";
              // Prediccion para torneo
              this->pred_G = 0;
              break;
    }
  // Caso en que no toma el salto VS prediccion
  } else {
    // Actualizacion de historia global
    this->Ghistory <<= 1;
    this->Ghistory[0] = 0;
    switch (state) {
      case STRONGLY_NOT_TAKEN:
              // Prediccion se mantiene en STRONGLY_NOT_TAKEN
              this->BTHg[dir_xor] = STRONGLY_NOT_TAKEN;
              // Suma 1 a numero correcto de not taken
              this->num_correct_not_taken = this->num_correct_not_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "N";
              // Prediccion para torneo
              this->pred_G = 0;
              break;
      case WEAKLY_NOT_TAKEN:
              // Prediccion pasa a STRONGLY_NOT_TAKEN
              this->BTHg[dir_xor] = this->BTHg[dir_xor] - 1;
              // Suma 1 a numero correcto de not taken
              this->num_correct_not_taken = this->num_correct_not_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "N";
              // Prediccion para torneo
              this->pred_G = 0;
              break;
      // Prediccion STRONGLY_TAKEN o WEAKLY_TAKEN
      default:
              // Prediccion pasa a WEAKLY_TAKEN o a WEAKLY_NOT_TAKEN
              this->BTHg[dir_xor] = this->BTHg[dir_xor] - 1;
              // Suma 1 a numero incorrecto de taken
              this->num_incorrect_taken = this->num_incorrect_taken + 1;
              // Prediccion incorrecta
              this->correct_or_incorrect = "incorrect";
              // Prediccion
              this->prediction = "T";
              // Prediccion para torneo
              this->pred_G = 1;
              break;
    }
  }
  // Return vacio
  return;
}

/** @brief Predictor con historia Privada
*
*   @details Implementacion de predictor con historia Privada
*
*   @param pc      Direccion de la instruccion de salto
*   @param outcome Caracter que indica si se toma o no el salto
*/
void Predictor::Pshare(string pc, string outcome){
  // Contador de numero de saltos
  this->num_branch++;
  // Direccion de string a unsigned long long
  unsigned long long PC = stoull(pc);
  // Direccion de unsigned long long a 32 bits
  bitset<32> PC_32b(PC);
  // Se desplaza direccion para obtener los s bits pasados por parametro
  PC_32b <<= 32 - this->size_BTH;
  PC_32b >>= 32 - this->size_BTH;
  // Nueva direccion a int
  int dir_PC = (int)(PC_32b.to_ulong());
  // Acceso a estado en la direccion del registro PTH
  int Phistory_int = this->PTH[dir_PC];
  // Direccion de unsigned long long a 32 bits
  bitset<32> itb_Ph(Phistory_int);
  // Se desplaza historia global para obtener los ph bits pasados por parametro
  this->Phistory = itb_Ph;
  this->Phistory <<= 32 - this->size_Ph;
  this->Phistory >>= 32 - this->size_Ph;
  // XOR entre direccion e historia privada
  bitset<32> dir_xor_32b = (PC_32b ^= this->Phistory);
  // Nueva direccion a int
  int dir_xor = (int)(dir_xor_32b.to_ulong());
  // Acceso a estado en la direccion del registro BTHp
  int state = this->BTHp[dir_xor];
  // Caso en que se toma el salto VS prediccion
  if (outcome == "T") {
    // Actualizacion de historia privada
    this->Phistory <<= 1;
    this->Phistory[0] = 1;
    // Nueva historia a int
    int Phistory_int = (int)(Phistory.to_ulong());
    // Actualiza historia privada en registro PTH
    this->PTH[dir_PC] = Phistory_int;
    switch (state) {
      case STRONGLY_TAKEN:
              // Predictor se mantiene en STRONGLY_TAKEN
              this->BTHp[dir_xor] = STRONGLY_TAKEN;
              // Suma 1 a numero correcto de taken
              this->num_correct_taken = this->num_correct_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "T";
              // Prediccion para torneo
              this->pred_P = 1;
              break;
      case WEAKLY_TAKEN:
              // Predictor pasa a STRONGLY_TAKEN
              this->BTHp[dir_xor] = this->BTHp[dir_xor] + 1;
              // Suma 1 a numero correcto de taken
              this->num_correct_taken = this->num_correct_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "T";
              // Prediccion para torneo
              this->pred_P = 1;
              break;
      // Prediccion STRONGLY_NOT_TAKEN o WEAKLY_NOT_TAKEN
      default:
              // Prediccion pasa a WEAKLY_NOT_TAKEN o a WEAKLY_TAKEN
              this->BTHp[dir_xor] = this->BTHp[dir_xor] + 1;
              // Suma 1 a numero incorrecto de taken
              this->num_incorrect_not_taken = this->num_incorrect_not_taken + 1;
              // Prediccion incorrecta
              this->correct_or_incorrect = "incorrect";
              // Prediccion
              this->prediction = "N";
              // Prediccion para torneo
              this->pred_P = 0;
              break;
    }
  // Caso en que no toma el salto VS prediccion
  } else {
    // Actualizacion de historia privada
    this->Phistory <<= 1;
    this->Phistory[0] = 0;
    // Nueva historia a int
    int Phistory_int = (int)(Phistory.to_ulong());
    // Actualiza historia privada en registro PTH
    this->PTH[dir_PC] = Phistory_int;
    switch (state) {
      case STRONGLY_NOT_TAKEN:
              // Prediccion se mantiene en STRONGLY_NOT_TAKEN
              this->BTHp[dir_xor] = STRONGLY_NOT_TAKEN;
              // Suma 1 a numero correcto de not taken
              this->num_correct_not_taken = this->num_correct_not_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "N";
              // Prediccion para torneo
              this->pred_P = 0;
              break;
      case WEAKLY_NOT_TAKEN:
              // Prediccion pasa a STRONGLY_NOT_TAKEN
              this->BTHp[dir_xor] = this->BTHp[dir_xor] - 1;
              // Suma 1 a numero correcto de not taken
              this->num_correct_not_taken = this->num_correct_not_taken + 1;
              // Prediccion correcta
              this->correct_or_incorrect = "correct";
              // Prediccion
              this->prediction = "N";
              // Prediccion para torneo
              this->pred_P = 0;
              break;
      // Prediccion STRONGLY_TAKEN o WEAKLY_TAKEN
      default:
              // Prediccion pasa a WEAKLY_TAKEN o a WEAKLY_NOT_TAKEN
              this->BTHp[dir_xor] = this->BTHp[dir_xor] - 1;
              // Suma 1 a numero incorrecto de taken
              this->num_incorrect_taken = this->num_incorrect_taken + 1;
              // Prediccion incorrecta
              this->correct_or_incorrect = "incorrect";
              // Prediccion
              this->prediction = "T";
              // Prediccion para torneo
              this->pred_P = 1;
              break;
    }
  }
  // Return vacio
  return;
}

/** @brief Predictor por Torneo
*
*   @details Implementacion de predictor por Torneo
*
*   @param pc      Direccion de la instruccion de salto
*   @param outcome Caracter que indica si se toma o no el salto
*/
void Predictor::Tournament(string pc, string outcome){
  // Direccion de string a unsigned long long
  unsigned long long PC = stoull(pc);
  // Direccion de unsigned long long a 32 bits
  bitset<32> PC_32b(PC);
  // Se desplaza direccion para obtener los s bits pasados por parametro
  PC_32b <<= 32 - this->size_BTH;
  PC_32b >>= 32 - this->size_BTH;
  // Nueva direccion a int
  int dir_PC = (int)(PC_32b.to_ulong());
  // Acceso a estado en la direccion del registro MPT
  int metapredictor = this->MPT[dir_PC];
  // Caso en que se toma el salto VS prediccion
  if (outcome == "T") {
    switch (metapredictor) {
      case STRONGLY_PREFER_PSHARE:
        // Se dan ambas predicciones
        Gshare(pc, outcome);
        Pshare(pc, outcome);
        // Check de prediccion correcta en cada predictor
        if (this->pred_P != this->pred_G) {
          if (this->pred_P == 0) {
            // Pshare falla y Gshare acierta pasa a WEAKLY_PREFER_PSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] + 1;
          } else {
            // Pshare acierta y Gshare falla se mantiene en STRONGLY_PREFER_PSHARE
            this->MPT[dir_PC] = STRONGLY_PREFER_PSHARE;
          }
        }
        break;
      case WEAKLY_PREFER_PSHARE:
        // Se dan ambas predicciones
        Gshare(pc, outcome);
        Pshare(pc, outcome);
        // Check de prediccion correcta en cada predictor
        if (this->pred_P != this->pred_G) {
          if (this->pred_P == 0) {
            // Pshare falla y Gshare acierta pasa a WEAKLY_PREFER_GSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] + 1;
          } else {
            // Pshare acierta y Gshare falla pasa a STRONGLY_PREFER_PSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] - 1;
          }
        }
        break;
      case WEAKLY_PREFER_GSHARE:
        // Se dan ambas predicciones
        Pshare(pc, outcome);
        Gshare(pc, outcome);
        // Check de prediccion correcta en cada predictor
        if (this->pred_P != this->pred_G) {
          if (this->pred_G == 0) {
            // Gshare falla y Pshare acierta, metapredictor pasa a WEAKLY_PREFER_PSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] - 1;
          } else {
            // Gshare acierta y Pshare falla pasa a STRONGLY_PREFER_GSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] + 1;
          }
        }
        break;
      case STRONGLY_PREFER_GSHARE:
        // Se dan ambas predicciones
        Pshare(pc, outcome);
        Gshare(pc, outcome);
        // Check de prediccion correcta en cada predictor
        if (this->pred_P != this->pred_G) {
          if (this->pred_G == 0) {
            // Gshare falla y Pshare acierta, metapredictor pasa a WEAKLY_PREFER_GSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] - 1;
          } else {
            // Gshare acierta y Pshare falla se mantiene en STRONGLY_PREFER_GSHARE
            this->MPT[dir_PC] = STRONGLY_PREFER_GSHARE;
          }
        }
        break;
    }

  } else {
    // Caso en que no toma el salto VS prediccion
    switch (metapredictor) {
      case STRONGLY_PREFER_PSHARE:
        // Se dan ambas predicciones
        Gshare(pc, outcome);
        Pshare(pc, outcome);
        // Check de prediccion correcta en cada predictor
        if (this->pred_P != this->pred_G) {
          if (this->pred_P == 1) {
            // Pshare falla y Gshare acierta pasa a WEAKLY_PREFER_PSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] + 1;
          } else {
            // Pshare acierta y Gshare falla se mantiene en STRONGLY_PREFER_PSHARE
            this->MPT[dir_PC] = STRONGLY_PREFER_PSHARE;
          }
        }
        break;
      case WEAKLY_PREFER_PSHARE:
        // Se dan ambas predicciones
        Gshare(pc, outcome);
        Pshare(pc, outcome);
        // Check de prediccion correcta en cada predictor
        if (this->pred_P != this->pred_G) {
          if (this->pred_P == 1) {
            // Pshare falla y Gshare acierta pasa a WEAKLY_PREFER_GSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] + 1;
          } else {
            // Pshare acierta y Gshare falla pasa a STRONGLY_PREFER_PSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] - 1;
          }
        }
        break;
      case WEAKLY_PREFER_GSHARE:
        // Se dan ambas predicciones
        Pshare(pc, outcome);
        Gshare(pc, outcome);
        // Check de prediccion correcta en cada predictor
        if (this->pred_P != this->pred_G) {
          if (this->pred_G == 1) {
            // Gshare falla y Pshare acierta, metapredictor pasa a WEAKLY_PREFER_PSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] - 1;
          } else {
            // Gshare acierta y Pshare falla pasa a STRONGLY_PREFER_GSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] + 1;
          }
        }
        break;
      case STRONGLY_PREFER_GSHARE:
        // Se dan ambas predicciones
        Pshare(pc, outcome);
        Gshare(pc, outcome);
        // Check de prediccion correcta en cada predictor
        if (this->pred_P != this->pred_G) {
          if (this->pred_G == 1) {
            // Gshare falla y Pshare acierta, metapredictor pasa a WEAKLY_PREFER_GSHARE
            this->MPT[dir_PC] = this->MPT[dir_PC] - 1;
          } else {
            // Gshare acierta y Pshare falla se mantiene en STRONGLY_PREFER_GSHARE
            this->MPT[dir_PC] = 3;
          }
        }
        break;
    }
  }
  // Return vacio
  return;
}

/** @brief Salida de simulacion
*
*   @details Implementacion de la generador de archivo e impresion de resultados
*
*   @param PC      Direccion de la instruccion de salto
*   @param outcome Caracter que indica si se toma o no el salto
*   @param x       Permite escribir encabezado o resultados
*   @param o       Tipo de salida de simulacion pasado por parametros
*   @param bp      Tipo de prediccion pasado por parametros
*/
void Predictor::print_r(string PC, string Outcome, int x, int o, int bp){
  // Creacion de archivo con resultados de prediccion
  if (o == 1) {
    ofstream file;
    if (x == 1) {
      // Encabezado de archivo
      file.open(this->predictor_type);
      file << "PC" << "\t\t\t\t" << "  Outcome" << "\t\t" << "Prediction" << "\t" << "correct/incorrect\n";
      file.close();
    } else if (x == 2) {
      // Resultados de archivo
      ofstream file(this->predictor_type, ios::app);
      file << PC << "\t\t  " << Outcome << "\t\t    " << this->prediction << "\t\t\t\t\t\t" << this->correct_or_incorrect << "\n";
      file.close();
    }
  }
  if (x == 3) {
    if (bp == 3){
      // Ajuste para prediccion por torneo
      this->num_correct_taken = this->num_correct_taken/2;
      this->num_correct_not_taken = this->num_correct_not_taken/2;
      this->num_incorrect_taken = this->num_incorrect_taken/2;
      this->num_incorrect_not_taken = this->num_incorrect_not_taken/2;
      this->num_branch = this->num_branch/2;
    }
    // Resultados en terminal
    this->percentage = (float)(this->num_correct_taken + this->num_correct_not_taken)/this->num_branch *100;
    cout << "_____________________________________________________________" << endl;
    cout << "Prediction parameters:" << endl;
    cout << "_____________________________________________________________" << endl;
    cout << "Branch prediction type:                               " << this->predictor_type << endl;
    cout << "BHT size (entries):                                   " << this->entries_BTH << endl;
    cout << "Global history register size:                         " << this->size_Gh << endl;
    cout << "Private history register size:                        " << this->size_Ph << endl;
    cout << "_____________________________________________________________" << endl;
    cout << "Simulation results:" << endl;
    cout << "_____________________________________________________________" << endl;
    cout << "Number of branch:                                     " << this->num_branch << endl;
    cout << "Number of correct prediction of taken branches:       " << this->num_correct_taken << endl;
    cout << "Number of incorrect prediction of taken branches:     " << this->num_incorrect_taken << endl;
    cout << "Number of correct prediction of not taken branches:   " << this->num_correct_not_taken << endl;
    cout << "Number of incorrect prediction of not taken branches: " << this->num_incorrect_not_taken << endl;
    cout << "Percentage of correct predictions:                    " << this->percentage << endl;
  }
  // Return vacio
  return;
  }

  /** @brief Destructor del Predictor
  */
Predictor::~Predictor(){}
