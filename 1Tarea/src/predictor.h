#ifndef PREDICTOR_H
#define PREDICTOR_H

#include <iostream>
#include <stdio.h>
#include<stdlib.h>
#include <bits/stdc++.h>

using namespace std;

/** @author Laureano Marin
*   @class  Predictor
*   @brief Clase de predictor de saltos parametrizable
*   @details
*		Clase Predictor que contiene las funciones para el predictor_type
*   Bimodal, Gshare, Pshare y por Torneo. Ademas de una clase para
*   mostrar los resultados.
*
*/
class Predictor{
	public:
		/// Constructor
		Predictor(int s, int bp, int gh, int ph, int o);
		/// Predictor Bimodal
		void Bimodal(string PC, string outcome);
		/// Predictor Gshare
		void Gshare(string PC, string outcome);
		/// Predictor Pshare
		void Pshare(string PC, string outcome);
		/// Predictor por Torneo
		void Tournament(string PC, string outcome);
		/// Generador de archivo e impresion de resultados
		void print_r(string PC, string Outcome, int x, int o, int bp);
		/// Destructor
		~Predictor();
		/// Direccion de la instruccion de salto
		int PC;
		/// Caracter que indica si se toma o no el salto
		string outcome;
	private:
		/// Estado del predictor
		int state;
		/// Numero de saltos
		int num_branch;
		/// Numero de Taken correctos
    int num_correct_taken;
		/// Numero de Not Taken correctos
    int num_correct_not_taken;
		/// Numero de Taken incorrectos
    int num_incorrect_taken;
		/// Numero de Not Taken incorrectos
    int num_incorrect_not_taken;
		/// Porcentaje de predicciones correctas
		float percentage;
		/// Prediccion del predictor
		string prediction;
		/// Prediccion correcta o incorrecta
		string correct_or_incorrect;
		/// Entradas de la BTH
		int entries_BTH;
		/// Tamano de la BTH ingresado como parametros
		int size_BTH;
		/// Tamano del Global history ingresado como parametro
		int size_Gh;
		/// Tamano del Private history ingresado como parametro
		int size_Ph;
		/// Prediccion de Pshare para Torneo
		int pred_P;
		/// Prediccion de Gshare para Torneo
		int pred_G;
		/// Registro BTH para Gshare
		vector<int> BTHg;
		/// Registro BTH para Pshare
		vector<int> BTHp;
		/// Registro BTH para Bimodal
		vector<int> BTH;
		/// Registro PTH para Pshare
		vector<int> PTH;
		/// Registro MPT para Torneo
		vector<int> MPT;
		/// Historia de Gshare
		bitset<32> Ghistory;
		/// Historia de Pshare
		bitset<32> Phistory;
		/// Tipo de predictor
		string predictor_type;

	};

#endif
