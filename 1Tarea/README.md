# PREDICTORES DE SALTOS

Universidad de Costa Rica

Escuela de Ingeniería Eléctrica

Laureano Marín Benavides

## Objetivo:

Analizar el rendimiento de distintos esquemas de predicción de saltos por medio de simulaciones en un lenguaje de alto nivel.

## Descripción del Proyecto:

Implementación de un simulador parametrizable de distintos predictores en C++, que permite obtener métricas de rendimiento utilizando como entrada un trace donde se indica la dirección del salto así como el resultado de este.

Los distintos predictores implementados son:
- Predictor Bimodal
- Predictor con historia global
- Predictor con historia privada
- Predictor con torneo

Los estados de BTH son inicializados en strongly not taken y los estados del metapredictor son inicializados en strongly prefer pshare.

## INSTRUCCIONES PARA EJECUCIÓN (compilar y correr):

```
$ make
$ gunzip -c branch-trace-gcc.trace.gz | ./branch -s < # > -bp < # > -gh < # > -ph < # > -o < # >
```

## INSTRUCCIONES PARA ELIMINAR EJECUTABLE

```
$ make clean
```

## Parámetros de entrada < # >:

- s  : Tamaño de la tabla BTH

- bp : Tipo de predicción
      
    - 0 Bimodal
      
    - 1 Gshare
    
    - 2 Pshare
      
    - 3 Torneo

- gh : Tamaño del registro de historia global

- ph : Tamaño del registro de historia privada

- o  : Salida de simulación
      
    - 0 Resultados en consola
      
    - 1 Resultados en archivo y consola
